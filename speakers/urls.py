from speakers import views
from django.urls import path

app_name = "speakers"

urlpatterns = [
    path('create/', views.create_speak_request, name="create_request"),
    path('delete/<int:request_id>/', views.delete_request, name="delete_request"),
    path('action/', views.room_action, name="join_room"),
    path('send-mail/', views.send_speaker_mail, name="send_mail")
]
