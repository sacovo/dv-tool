import secrets
from adminsortable.models import SortableMixin
from django.conf import settings

from events.models import Event
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.


class AgendaPoint(SortableMixin):
    event = models.ForeignKey(Event, models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=255)
    order = models.IntegerField(default=0, editable=False, db_index=True)

    speakers_allowed = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['order']
        verbose_name = _("Traktandenpunkt")
        verbose_name_plural = _("Traktandenpunkte")


REQUEST_SUBMITTED = 0
REQUEST_SCHEDULED = 1
REQUEST_WAITING = 2
REQUEST_READY = 3
REQUEST_SPEAKING = 4
REQUEST_DONE = 5

REQUEST_STATES = (
    (REQUEST_SUBMITTED, _("eingereicht")), # Speaker submitted request
    (REQUEST_SCHEDULED, _("geplant")),     # Request was scheduled
    (REQUEST_WAITING, _("wartend")),       # Operator is waiting for speaker
    (REQUEST_READY, _("bereit")),          # Speaker in line
    (REQUEST_SPEAKING, _("am sprechen")),  # Speaker is speaking
    (REQUEST_DONE, _("fertig")),           # Speaker is done
)


def request_states(request):
    return {
        'SCHEDULED': REQUEST_SCHEDULED,
        'WAITING': REQUEST_WAITING,
        'READY': REQUEST_READY,
        'SPEAKING': REQUEST_SPEAKING,
        'DONE': REQUEST_DONE,
    }


REASONS = (
    ("Unterstützung der Stellungnahme GL / Soutien de la prise de position du CD / A favore del parere del comitato direttivo",
    ) * 2,
    ("Bekämpfung der Stellungnahme GL / Rejet de la prise de position du CD / Contro il parere del comitato direttivo",
    ) * 2,
    ("Aufrechterhaltung des Antrags / Maintien de la proposition / Mantenere la proposta",
    ) * 2,
    ("Rückzug des Antrags / Retrait de la proposition / Ritirare la proposta",) * 2,
    ("Ordnungsantrag / Motion d'ordre / Mozione d’ordine",) * 2,
)


class SpeakRequest(SortableMixin):
    point = models.ForeignKey(AgendaPoint, models.CASCADE, verbose_name=_("Traktandum"))
    speaker = models.ForeignKey(get_user_model(), models.CASCADE, verbose_name=_("Sprecher*in"))

    link = models.CharField(max_length=255, default=secrets.token_urlsafe, verbose_name=_("Link"))

    order = models.IntegerField(default=0, editable=False, db_index=True, verbose_name=_("Reihenfolge"))
    state = models.IntegerField(choices=REQUEST_STATES, default=0, verbose_name=_("Status"))

    station = models.IntegerField(default=-1, verbose_name=_("Station"))

    language = models.CharField(max_length=2, choices=settings.LANGUAGES, default='de', verbose_name=_("Sprache"))
    phone_number = models.CharField(max_length=30, verbose_name=_("Telefon"))

    reason = models.CharField(max_length=255, default=REASONS[0][0], choices=REASONS, verbose_name=_("Wortmeldung"))
    comment = models.CharField(max_length=255, blank=True, verbose_name=_("Kommentar (Welcher Antrag bei PP, ...)"))
    summary = models.TextField(blank=True, verbose_name=_("Zusammenfassung (für die Übersetzung)"))

    gender = models.CharField(max_length=30, blank=True, verbose_name=_("Gender"))

    def __str__(self):
        return f"{self.point} - {self.speaker}"

    def full_name(self):
        return self.speaker.get_full_name()

    def save(self, *args, **kwargs):
        if self.state == REQUEST_SUBMITTED and self.station != -1:
            self.state = REQUEST_SCHEDULED
        super().save(*args, **kwargs)

    def jitsi_link(self):
        return 'https://' + settings.JITSI_DOMAIN + '/' + settings.JITSI_PREFIX + self.link

    class Meta:
        ordering = ['order']
        verbose_name = _("Wortmeldung")
        verbose_name_plural = _("Wortmeldungen")


class MailConfiguration(models.Model):
    subject = models.CharField(max_length=255)
    content = models.TextField()
