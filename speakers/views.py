import json
from django.conf import settings

from django.core.mail import send_mail
from speakers.models import AgendaPoint, REQUEST_WAITING, SpeakRequest
from django.http.response import JsonResponse
from speakers.forms import SpeakerForm
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


@csrf_exempt
@login_required
def create_speak_request(request):
    event = AgendaPoint.objects.get(pk=request.POST.get('point')).event
    form = SpeakerForm(request.POST, event=event)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.speaker = request.user
        instance.state = 0
        instance.save()
        return JsonResponse({'status': 'ok'})

    return JsonResponse({'status': 'error'})


@csrf_exempt
@login_required
def delete_request(request, request_id):
    speaker_request = SpeakRequest.objects.get(pk=request_id)

    if speaker_request.speaker == request.user:
        speaker_request.delete()

        return JsonResponse({'status': 'ok'})

    return JsonResponse({'status': "error"})


@csrf_exempt
@user_passes_test(lambda u: u.is_staff)
def room_action(request):
    data = json.loads(request.body)
    speaker_request = SpeakRequest.objects.get(id=data['id'])

    speaker_request.state = data['action']
    speaker_request.save()

    return JsonResponse({'state': 'ok'})


@csrf_exempt
@login_required
def send_speaker_mail(request):
    data = json.loads(request.body)
    speaker_request = SpeakRequest.objects.get(id=data['id'])

    event = speaker_request.point.event
    mail_config = event.mail_config

    subject = mail_config.subject.format(
        point=speaker_request.point,
        event=event,
        first_name=speaker_request.speaker.first_name,
        last_name=speaker_request.speaker.last_name,
    )

    content = mail_config.content.format(
        point=speaker_request.point,
        event=event,
        first_name=speaker_request.speaker.first_name,
        last_name=speaker_request.speaker.last_name,
        link=speaker_request.jitsi_link(),
    )

    send_mail(subject, content, event.invite_mail_sender, [speaker_request.speaker.email])

    return JsonResponse({'state': 'ok'})
