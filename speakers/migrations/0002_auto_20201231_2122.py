# Generated by Django 3.1.4 on 2020-12-31 20:22

from django.db import migrations, models
import django.db.models.deletion
import secrets


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
        ('speakers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='speakrequest',
            name='event',
        ),
        migrations.AddField(
            model_name='agendapoint',
            name='event',
            field=models.ForeignKey(blank=True,
                                    null=True,
                                    on_delete=django.db.models.deletion.CASCADE,
                                    to='events.event'),
        ),
        migrations.AddField(
            model_name='speakrequest',
            name='link',
            field=models.CharField(default=secrets.token_urlsafe, max_length=255),
        ),
        migrations.AlterField(
            model_name='speakrequest',
            name='state',
            field=models.IntegerField(choices=[(0, 'submitted'), (
                1, 'scheduled'), (2, 'waiting'), (3, 'speaking'), (4, 'done')]),
        ),
    ]
