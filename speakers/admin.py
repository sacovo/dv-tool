from adminsortable.admin import NonSortableParentAdmin, SortableAdmin, SortableTabularInline
from django.contrib import admin
from speakers.models import AgendaPoint, MailConfiguration, SpeakRequest

# Register your models here.


class AgendaInline(SortableTabularInline):
    model = AgendaPoint
    fields = ['title', 'speakers_allowed']
    extra = 0


class SpeakRequestInline(SortableTabularInline):
    model = SpeakRequest
    fields = ['full_name', 'gender', 'state', 'language', 'reason', 'comment', 'station']
    readonly_fields = ['full_name', 'speaker', 'gender']
    extra = 0


@admin.register(AgendaPoint)
class AgendaPointAdmin(SortableAdmin):
    list_display = ['title']
    fields = ['title', 'speakers_allowed', 'event']

    list_filter = ['event']

    search_fields = ['title']

    inlines = [SpeakRequestInline]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_authenticated:
            return qs

        return qs.filter(event__managers=request.user)


@admin.register(MailConfiguration)
class MailConfigurationAdmin(admin.ModelAdmin):
    list_display = ['subject']


@admin.register(SpeakRequest)
class SpeakRequestAdmin(SortableAdmin):
    list_display = [
        'point', 'full_name', 'language', 'reason', 'comment', 'state', 'station'
    ]
    list_filter = ['point__event', 'point', 'speaker']
    fields = [
        'point', 'speaker', 'jitsi_link', 'station', 'phone_number', 'language', 'reason',
        'comment', 'summary', 'gender'
    ]
    list_editable = ['station']
    readonly_fields = ['state', 'jitsi_link']
    autocomplete_fields = ['point']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(point__event__managers=request.user)
