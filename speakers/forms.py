from speakers.models import SpeakRequest
from django import forms


class SpeakerForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        event = kwargs.pop('event', None)
        super().__init__(*args, **kwargs)

        if event:
            self.fields['point'].queryset = event.agendapoint_set.filter(
                speakers_allowed=True)
            if not event.need_summary:
                del self.fields['summary']
            if not event.need_reason:
                del self.fields['reason']
            if not event.need_gender:
                del self.fields['gender']

    class Meta:
        fields = [
            'point', 'language', 'phone_number', 'reason', 'comment', 'gender', 'summary'
        ]
        model = SpeakRequest
