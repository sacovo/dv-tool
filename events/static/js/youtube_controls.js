function onYouTubeIframeAPIReady() {
  const ytVideos = document.getElementsByClassName("yt-video");
  for (let i = 0; i < ytVideos.length; i++) {
    console.log(ytVideos[i]);
    player = new YT.Player(ytVideos[i].id, {
      events: {
        onReady: (event) => console.log(event),
        onStateChange: onPlayerStateChange,
      },
    });
  }
}
function onPlayerStateChange() {
  console.log("Hello");
}
