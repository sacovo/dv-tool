var app = new Vue({
  el: "#app",
  data: {
    info: {},
    api: null,
    messages: {},
    updateScrolls: [],
    rooms: {},
    selectedRoom: undefined,
    members: [],
    loading: false,
    memberSelect: undefined,
    users: [],
    sentAlerts: [],
    ytPlayers: [],
    choices: null,
    oldChoices: null,
  },
  delimiters: ["[[", "]]"],

  mounted: function () {
    setInterval(() => {
      this.fetchData();
    }, 5000);

    this.fetchData();
  },

  updated: function () {
    this.updateScrolls.forEach((element) => element.scrollTo(0, element.scrollHeight + element.clientHeight));
    this.updateScrolls = [];
  },

  methods: {
    fetchData: function () {
      if (this.loading) {
        return;
      }
      this.loading = true;
      axios
        .get("info/", {
          params: { timestamp: this.info ? this.info.timestamp : "" },
        })
        .catch((error) => {
          console.log(error);
          this.loading = false;
        })
        .then((response) => {
          this.info = response.data;
          this.drawVotes();
          this.updateChat();
          this.checkWaitingCall();
          this.updateChoices();
          this.loading = false;
        });
      /*
      axios.get("/chat/rooms/").then((response) => {
        this.rooms = response.data;
      });
      */
    },
    addYtPlayer: function (player) {
      this.ytPlayers.push(player);
    },
    checkWaitingCall: function () {
      let waitingCalls = this.info.speakerRequests.filter((item) => item.state === 2);
      let notNotified = waitingCalls.filter((item) => !this.sentAlerts.includes(item.id));
      if (notNotified.length > 0) {
        let request = notNotified[0];
        this.sentAlerts.push(request.id);
        alert(document.getElementById("waitingCallMessage").dataset.value);
      }
    },
    drawVotes: function () {
      d3.select("#votes").html("");

      d3.select("#votes")
        .selectAll("div")
        .data(this.info.votes)
        .enter()
        .append("div")
        .attr("id", (d) => d.pk)
        .attr("data-vote", (d) => d.vote)
        .attr("data-self", (d) => d.pk == this.info.user_id)
        .attr("title", (d) => d.name);
    },
    updateChat: function () {
      const messageElements = document.getElementsByClassName("messages");
      const scrollUpdateElements = [];

      for (let i = 0; i < messageElements.length; i++) {
        const element = messageElements[i];
        if (element.scrollHeight - element.scrollTop === element.clientHeight) {
          scrollUpdateElements.push(element);
        }
      }
      this.updateScrolls = scrollUpdateElements;

      while (this.info.messages.length > 0) {
        const message = this.info.messages.pop();
        const stack = this.messages[message.channel];
        if (stack) {
          stack.push(message);
        } else {
          this.messages[message.channel] = [message];
        }
      }
    },
    updateChoices: function () {
      if (document.getElementById("id_point") && this.choices === null) {
        this.choices = new Choices(document.getElementById("id_point"), { shouldSort: false });
      }

      if (this.oldChoices !== null && JSON.stringify(this.oldChoices) === JSON.stringify(this.info.openPoints)) return;

      this.choices.setChoices(
        this.info.openPoints.map((x) => {
          return { value: x.id, label: x.title };
        }),
        "value",
        "label",
        true
      );
      this.oldChoices = this.info.openPoints;
    },
    voteAction: _.throttle(function (event) {
      axios
        .post("vote/", {
          vote: event.target.dataset.value,
        })
        .then((response) => {
          this.info = response.data;
          this.drawVotes();
        });
    }, 600),
    formatDate: function (date) {
      return `${date.getHours().toString().padStart(2, "0")}:${date.getMinutes().toString().padStart(2, "0")}`;
    },

    submitForm: function (event) {
      const form = event.target;
      fetch(form.action, {
        method: "POST",
        body: new FormData(form),
      }).then((response) => {
        form.reset();
        this.oldChoices = null;
        this.fetchData();
      });
    },

    deleteAction: function (event) {
      const id = event.target.dataset.id;
      if (confirm(document.getElementById("redactConfirm").dataset.value)) {
        axios.post(event.target.dataset.link + id + "/").then((response) => this.fetchData());
      }
    },

    joinMeeting: function (event) {
      event.preventDefault();
      if (this.api) {
        alert(document.getElementById("hangupMessage").dataset.value);
        return;
      }
      if (!this.sentAlerts.includes(Number(event.target.dataset.id))) {
        this.sentAlerts.push(Number(event.target.dataset.id));
      }
      const jitsiSettings = document.getElementById("jitsiSettings");

      const options = {
        roomName: jitsiSettings.dataset.prefix + event.target.dataset.link,
        parentNode: document.querySelector("#meet"),
        userInfo: {
          displayName: document.getElementById("nameInput").dataset.value,
        },
      };

      this.api = new JitsiMeetExternalAPI(jitsiSettings.dataset.domain, options);

      this.api.on("videoConferenceJoined", () => {
        this.api.executeCommand("subject", event.target.title);
      });

      this.api.addEventListener("incomingMessage", (event) => {
        if (event.message === "pause") {
          this.ytPlayers.forEach((player) => player.pauseVideo());
        }
      });

      this.api.on("readyToClose", () => {
        d3.select("#meet").html("");
        this.api = null;
      });
    },

    sendMessage: function (event) {
      const channel = event.target.dataset.channel;
      const input = document.getElementById(`input-${channel}`);
      const message = input.value;
      input.value = "";
      if (message.trim().length < 1) {
        return;
      }
      axios
        .post("message/", {
          channel: channel,
          message: checkText(message),
        })
        .then((response) => this.fetchData());
    },

    truncate: function (string, length) {
      if (string.length <= length) {
        return string;
      }
      return string.substring(0, length - 3) + "...";
    },
  },
});

function onYouTubeIframeAPIReady() {
  const ytVideos = document.getElementsByClassName("yt-video");
  for (let i = 0; i < ytVideos.length; i++) {
    player = new YT.Player(ytVideos[i].id, {});
    app.addYtPlayer(player);
  }
}
