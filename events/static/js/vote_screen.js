var app = new Vue({
  el: "#app",
  data: {
    info: {},
  },
  delimiters: ["[[", "]]"],

  mounted: function () {
    setInterval(() => {
      this.fetchData();
    }, 1000);
    this.fetchData();
  },

  methods: {
    fetchData: function () {
      axios
        .get("info/")
        .catch((error) => {
          console.log(error);
        })
        .then((response) => {
          this.info = response.data;
          this.drawVotes();
        });
    },
    drawVotes: function () {
      d3.select("#votes").html("");

      d3.select("#votes")
        .selectAll("div")
        .data(this.info.votes)
        .enter()
        .append("div")
        .attr("id", (d) => d.pk)
        .attr("data-vote", (d) => d.vote)
        .attr("data-self", (d) => d.pk == this.info.user_id);
    },
  },
});
