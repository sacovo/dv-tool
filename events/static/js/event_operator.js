var app = new Vue({
  el: "#app",
  data: {
    info: {},
    api: null,
  },
  delimiters: ["[[", "]]"],

  mounted: function () {
    setInterval(() => {
      this.fetchData();
    }, 5000);
    this.fetchData();
  },

  methods: {
    joinMeeting: function (event) {
      event.preventDefault();
      if (this.api) {
        return;
      }
      const jitsiSettings = document.getElementById("jitsiSettings");

      const options = {
        roomName: jitsiSettings.dataset.prefix + event.target.dataset.link,
        parentNode: document.querySelector("#meet"),
        userInfo: {
          displayName: "Hello",
        },
      };

      this.api = new JitsiMeetExternalAPI(jitsiSettings.dataset.domain, options);

      this.api.on("videoConferenceJoined", (event) => {
        this.api.executeCommand("subject", event.target.dataset.subject);
      });

      axios
        .post("/speakers/action/", {
          action: event.target.dataset.next,
          id: event.target.dataset.id,
        })
        .then(() => {
          this.fetchData();
        });

      this.api.on("readyToClose", () => {
        d3.select("#meet").html("");
        this.api = null;
      });
    },
    requestsForOperator: function (operatorNr) {
      if (this.info.speakerRequests) {
        return this.info.speakerRequests.filter((item) => item.station == operatorNr);
      }
      return [];
    },
    roomAction: function (event) {
      axios
        .post("/speakers/action/", {
          action: event.target.dataset.next,
          id: event.target.dataset.id,
        })
        .then(() => this.fetchData());
    },
    sendMail: function (event) {
      axios.post("/speakers/send-mail/", {
        id: event.target.dataset.id,
      });
    },
    fetchData: function () {
      axios.get("info/").then((response) => {
        this.info = response.data;
      });
    },
  },
});
