var app = new Vue({
  el: "#app",
  data: {
    info: {},
    messages: {},
    updateScrolls: [],
  },
  delimiters: ["[[", "]]"],

  mounted: function () {
    setInterval(() => {
      this.fetchData();
    }, 5000);
    this.fetchData();
  },
  updated: function () {
    this.updateScrolls.forEach((element) => element.scrollTo(0, element.scrollHeight + element.clientHeight));
  },

  methods: {
    drawVotes: function () {
      d3.select("#votes").html("");

      d3.select("#votes")
        .selectAll("div")
        .data(this.info.votes)
        .enter()
        .append("div")
        .attr("id", (d) => d.pk)
        .attr("data-vote", (d) => d.vote)
        .attr("data-self", (d) => d.pk == this.info.user_id);
    },
    formatDate: function (date) {
      return `${date.getHours().toString().padStart(2, "0")}:${date.getMinutes().toString().padStart(2, "0")}`;
    },
    fetchData: function () {
      axios
        .get("info/", {
          params: { timestamp: this.info ? this.info.timestamp : "" },
        })
        .then((response) => {
          this.info = response.data;
          this.drawVotes();
          this.updateChat();
        });
    },
    updateChat: function () {
      const messageElements = document.getElementsByClassName("messages");
      const scrollUpdateElements = [];

      for (let i = 0; i < messageElements.length; i++) {
        const element = messageElements[i];
        if (element.scrollHeight - element.scrollTop === element.clientHeight) {
          scrollUpdateElements.push(element);
        }
      }
      this.updateScrolls = scrollUpdateElements;

      while (this.info.messages.length > 0) {
        const message = this.info.messages.pop();
        const stack = this.messages[message.channel];
        if (stack) {
          stack.push(message);
        } else {
          this.messages[message.channel] = [message];
        }
      }
    },
  },
});
