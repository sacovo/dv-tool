from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db.models.expressions import Value
from django.db.models.functions.text import Concat
from chat.forms import RoomForm
from chat.models import Message
import json
from speakers.forms import SpeakerForm
from speakers.models import REQUEST_DONE, REQUEST_READY, REQUEST_SCHEDULED, REQUEST_SPEAKING, REQUEST_WAITING, SpeakRequest
from delegates.models import INVITE_PROCESSED, Invite, VOTE_ABSTAIN, VOTE_NO, VOTE_YES
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.html import escape, urlize
from django.http.response import HttpResponse, JsonResponse
from django.utils import timezone
from events.models import Event
from django.db.models import F
from votes.models import PUBLIC_FIELDS, Ballot, get_open_vote_dict
from events.forms import MessageForm, OperatorForm
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


@login_required
def event_list(request):
    if request.user.is_staff:
        events = Event.objects.all()
    else:
        events = Event.objects.filter(invite__user=request.user)

    return render(request, "events/event_list.html", {'events': events})


def event_detail(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    speaker_form = SpeakerForm(event=event)
    room_form = RoomForm()

    template_name = "events/event_detail_simple.html" if event.is_simple else "events/event_detail.html"

    return render(
        request, template_name, {
            'event':
                event,
            'speaker_form':
                speaker_form,
            'can_vote':
                request.user.is_authenticated
                and event.invite_set.filter(user=request.user).exists(),
            'can_speak':
                request.user.is_authenticated,
            'room_form':
                room_form,
            'user_list':
                get_user_model().objects.all(),
        })


@user_passes_test(lambda u: u.is_staff)
def event_admin(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    if not request.user in event.managers.all():
        raise PermissionDenied("permission denied.")

    return render(request, "events/event_admin.html", {'event': event})


def message_list(messages):
    return list(
        messages.annotate(first_name=F("user__first_name"),
                          last_name=F("user__last_name")).values(
                              'channel', 'message', 'first_name', 'last_name', 'user_id',
                              'timestamp'))


def event_info(request, event_pk):
    event = Event.objects.get(pk=event_pk)
    event_info = {}
    try:
        invite = event.invite_set.get(user=request.user, state=INVITE_PROCESSED)
        event_info['user_id'] = invite.pk
        event_info['user_vote'] = invite.vote
        event_info['can_vote'] = invite.can_vote

    except (Invite.DoesNotExist, TypeError):
        event_info['user_id'] = None
        event_info['user_vote'] = None
        event_info['can_vote'] = False

    event_info.update(get_open_vote_dict(event))

    votes = event.invite_set.filter(state=INVITE_PROCESSED, can_vote=True).annotate(
        name=Concat("user__first_name", Value(" "), "user__last_name"))

    event_info['votes'] = list(
        votes.values(*((
            'pk',
            'vote',
            'name',
        ) if request.user.is_authenticated else ('pk', 'vote'))))

    if request.user.is_authenticated:
        event_info['ballots'] = list(
            event.ballot_set.filter(processed=True).order_by('-order')[:5].values(
                'title', 'yes', 'no', 'abstain'))
        speak_requests = SpeakRequest.objects.filter(
            speaker=request.user,
            point__event=event).annotate(title=F("point__title")).order_by(
                'point__order', 'order')
        event_info['speakerRequests'] = [{
            'title': s.title,
            'id': s.id,
            'link': s.link,
            'state': s.state,
            'state_display': s.get_state_display(),
        } for s in speak_requests]

        event_info['openPoints'] = list(
            event.agendapoint_set.filter(speakers_allowed=True).order_by('order').values(
                'title', 'id'))

        event_info['timestamp'] = timezone.now()

        if timestamp := request.GET.get('timestamp'):
            event_info['messages'] = message_list(
                Message.objects.filter(event=event, timestamp__gte=timestamp))
        else:
            event_info['messages'] = message_list(
                Message.objects.filter(event=event)[:20])
    else:
        event_info['messages'] = list()

    return JsonResponse(event_info)


@user_passes_test(lambda u: u.is_staff)
def event_admin_info(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    if not request.user in event.managers.all():
        raise PermissionDenied("no admin access to this event.")

    event_info = {}

    event_info['votes'] = list(
        event.invite_set.filter(state=INVITE_PROCESSED).values('pk', 'vote'))

    speak_requests = SpeakRequest.objects.filter(
        point__event=event, state__lt=5).annotate(
            title=F("point__title"),
            username=F("speaker__username"),
            first_name=F("speaker__first_name"),
            last_name=F("speaker__last_name")).order_by('point__order', 'order')

    event_info['speakerRequests'] = [{
        'title':
            s.title,
        'name': ((s.first_name + " " + s.last_name) if s.first_name else
                 (s.username)) + ", " + str(s.speaker.groups.first() or ''),
        'reason': (s.reason if event.need_reason else '') +
                  (f" ({s.comment})" if s.comment else "") +
                  ((", " + s.gender) if s.gender else ""),
        'state':
            s.state,
        'state_display':
            s.get_state_display()
    } for s in speak_requests]

    event_info.update(get_open_vote_dict(event))

    event_info['ballots'] = list(
        event.ballot_set.filter(processed=True).order_by('-order').values(*PUBLIC_FIELDS))

    event_info['timestamp'] = timezone.now()

    if timestamp := request.GET.get('timestamp'):
        event_info['messages'] = message_list(
            Message.objects.filter(event=event, timestamp__gte=timestamp))
    else:
        event_info['messages'] = message_list(Message.objects.filter(event=event)[:100])

    return JsonResponse(event_info)


@csrf_exempt
@login_required
def event_vote(request, event_pk):
    invite = Event.objects.get(pk=event_pk).invite_set.get(
        user=request.user,
        state=INVITE_PROCESSED,
        can_vote=True,
    )
    data = json.loads(request.body)
    invite.vote = data['vote']

    invite.save()

    return event_info(request, event_pk)


@login_required
def event_vote_results(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    return JsonResponse({
        'data': list(event.ballot_set.filter(processed=True).values(*PUBLIC_FIELDS)),
    })


@user_passes_test(lambda u: u.is_staff)
def operator_view(request, event_pk, operator):
    event = Event.objects.get(pk=event_pk)

    if not request.user in event.operators.all():
        raise PermissionDenied()

    return render(request, "events/operator.html", {
        'event': event,
        'operator': operator,
    })


@user_passes_test(lambda u: u.is_staff)
def operator_info(request, event_pk, operator):
    event = Event.objects.get(pk=event_pk)

    if not request.user in event.operators.all():
        raise PermissionDenied()

    speak_requests = SpeakRequest.objects.filter(
        point__event=event,
        state__lt=5,
        state__gt=0,
    ).annotate(
        title=F("point__title"),
        username=F('speaker__username'),
        first_name=F("speaker__first_name"),
        last_name=F("speaker__last_name"),
    ).order_by('point__order', 'order')
    event_info = {}

    event_info['speakerRequests'] = [{
        'title': s.title,
        'name': (s.first_name + " " + s.last_name) if s.first_name else s.username,
        'link': s.link,
        'phone': s.phone_number,
        'mail': s.speaker.email,
        'state': s.state,
        'station': s.station,
        'state_display': s.get_state_display(),
        'id': s.id,
    } for s in speak_requests]

    return JsonResponse(event_info)


@user_passes_test(lambda u: u.is_staff)
def operator_start(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    event = Event.objects.get(pk=event_pk)

    if not request.user in event.operators.all():
        raise PermissionDenied()

    form = OperatorForm(request.GET)

    if form.is_valid():
        return redirect("events:operator_view", event.pk,
                        form.cleaned_data['operator_nr'])

    return render(request, 'events/operator_start.html', {
        'event': event,
        'form': form,
    })


def convert_message(msg: str) -> str:
    if msg.startswith("https://media.giphy.com/") and msg.endswith(".gif"):
        return "<img src=\"{src}\"/>".format(src=escape(msg))
    return urlize(msg, autoescape=True)


@csrf_exempt
@login_required
def chat_message(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    data = json.loads(request.body)

    Message.objects.create(event=event,
                           channel=data['channel'],
                           user=request.user,
                           message=convert_message(data['message'])[:280])

    return JsonResponse({'status': "ok"})


@login_required
def vote_screen(request, event_pk):
    event = Event.objects.get(pk=event_pk)
    return render(request, 'events/vote_screen.html', {'event': event})


@login_required
def vote_screen_info(request, event_pk):
    event = Event.objects.get(pk=event_pk)
    event_info = {}

    event_info['votes'] = list(
        event.invite_set.filter(state=INVITE_PROCESSED).values('pk', 'vote'))
    event_info.update(get_open_vote_dict(event))
    event_info['total'] = event_info['vote_yes'] + event_info['vote_no'] + event_info[
        'vote_abstain']

    return JsonResponse(event_info)
