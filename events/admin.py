from speakers.admin import AgendaInline
from votes.admin import BallotInline
from django.contrib import admin
from events import models, tasks
from adminsortable.admin import NonSortableParentAdmin
# Register your models here.


class VideoLinkInline(admin.TabularInline):
    extra = 0
    model = models.VideoLink


class ChatWindowInline(admin.TabularInline):
    extra = 0
    model = models.ChatWindow


@admin.register(models.Event)
class EventAdmin(NonSortableParentAdmin):
    inlines = (VideoLinkInline, ChatWindowInline, BallotInline, AgendaInline)

    search_fields = ['title']

    list_display = ('title', 'date', 'is_open')

    actions = (
        'process_invites',
        'send_invites',
        'send_invites_pw',
        'reset_invites',
        'send_reminder',
    )

    filter_horizontal = ['managers', 'operators']
    readonly_fields = ['id']

    def process_invites(self, request, queryset):
        for event in queryset:
            tasks.process_invites.delay(event.pk)

    def send_invites(self, request, queryset):
        for event in queryset:
            tasks.send_invites.delay(event.pk)

    def send_invites_pw(self, request, queryset):
        for event in queryset:
            tasks.send_invites_pw.delay(event.pk)

    def send_reminder(self, request, queryset):
        for event in queryset:
            tasks.send_reminder.delay(event.pk)

    def send_reminder_with_pw(self, request, queryset):
        for event in queryset:
            tasks.send_invites_pk(event.pk)

    def reset_invites(self, request, queryset):
        for event in queryset:
            tasks.reset_invites.delay(event.pk)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(managers=request.user)
