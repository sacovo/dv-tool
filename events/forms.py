from django import forms
from django.forms import widgets
from multiupload.fields import MultiFileField


class MessageForm(forms.Form):
    subject = forms.CharField()

    content = forms.CharField(widget=widgets.Textarea)

    attachments = MultiFileField(min_num=0, max_num=5)


class OperatorForm(forms.Form):
    operator_nr = forms.IntegerField()
