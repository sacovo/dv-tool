"""
Shared tasks for calculating projections
"""
from events.models import Event
from celery import shared_task


@shared_task
def process_invites(event_pk):
    Event.objects.get(pk=event_pk).process_invites()


@shared_task
def send_invites(event_pk):
    Event.objects.get(pk=event_pk).send_invites()


@shared_task
def send_invites_pw(event_pk):
    Event.objects.get(pk=event_pk).send_invites_with_pw()


@shared_task
def reset_invites(event_pk):
    Event.objects.get(pk=event_pk).reset_invites()


@shared_task
def send_invites_pk(event_pk):
    Event.objects.get(pk=event_pk).send_invites_with_pw()


@shared_task
def send_reminder(event_pk):
    Event.objects.get(pk=event_pk).send_reminder()
