from events import views
from django.urls import path

app_name = "events"

urlpatterns = (
    path("", views.event_list, name="event_list"),
    path("<uuid:event_pk>/", views.event_detail, name="event_detail"),
    path("<uuid:event_pk>/info/", views.event_info, name="event_info"),
    path("<uuid:event_pk>/vote/", views.event_vote, name="event_vote"),
    path("<uuid:event_pk>/screen/", views.vote_screen, name="vote_screen"),
    path("<uuid:event_pk>/screen/info/", views.vote_screen_info, name="vote_screen_info"),
    path("<uuid:event_pk>/message/", views.chat_message, name="send_message"),
    path("<uuid:event_pk>/results/", views.event_vote_results, name="event_vote_results"),
    path("<uuid:event_pk>/admin/", views.event_admin, name="event_admin"),
    path("<uuid:event_pk>/operator/", views.operator_start, name="operator_start"),
    path("<uuid:event_pk>/operator/<int:operator>/",
         views.operator_view,
         name="operator_view"),
    path("<uuid:event_pk>/operator/<int:operator>/info/",
         views.operator_info,
         name="operator_info"),
    path("<uuid:event_pk>/admin/info/", views.event_admin_info, name="event_admin_info"),
)
