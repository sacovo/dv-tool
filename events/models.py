from csv import DictReader
import secrets
import uuid

from django.contrib.auth import get_user_model
from delegates.models import INVITE_CREATED, INVITE_PROCESSED, INVITE_SENT, Invite
from django.core.mail import send_mail, send_mass_mail
from django.contrib.auth.models import Group
from django.db import models
from django.utils.translation import gettext as _

from django.core.files.storage import FileSystemStorage
from django.conf import settings

upload_storage = FileSystemStorage(location=settings.UPLOAD_ROOT, base_url='/uploads')

# Create your models here.


class Event(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    title = models.CharField(max_length=255, verbose_name=_("Titel"))
    date = models.DateField(verbose_name=_("Datum"))

    invite_mail_sender = models.CharField(max_length=255, verbose_name=_("Einlaundgsmail Absender*in"))
    invite_mail_subject = models.CharField(max_length=255, verbose_name=_("Einlaundgsmail Betreff"))
    invite_mail_template = models.TextField(verbose_name=_("Einlaundgsmail Vorlage"), help_text=_("Text, den die Delegierten erhalten, Platzhalter für den Code: {code}, für die id des Events: {id}"))

    reminder_mail_subject = models.CharField(max_length=255, verbose_name=_("Erinnerungsmail Betreff"), blank=True)
    reminder_mail_template = models.TextField(verbose_name=_("Erinnerungsmail Vorlage"), help_text=_("Text, den die Delegierten erhalten, Platzhalter für den Code: {code}, für die id des Events: {id}"), blank=True)

    is_open = models.BooleanField(default=False, verbose_name=_("ist offen"))
    is_simple = models.BooleanField(default=False, verbose_name=_("Simpel"))
    need_summary = models.BooleanField(default=False)
    need_reason = models.BooleanField(default=False)
    need_gender = models.BooleanField(default=False)

    mail_file = models.FileField(verbose_name=_("E-Mail Datei"), storage=upload_storage)

    embed_code = models.TextField(blank=True)

    mail_config = models.ForeignKey(
        "speakers.MailConfiguration",
        models.SET_NULL,
        null=True,
        blank=True,
    )

    managers = models.ManyToManyField(get_user_model())
    operators = models.ManyToManyField(get_user_model(), related_name="+")

    def process_invites(self):
        self.reset_invites()
        with self.mail_file.open('r') as f:
            reader = DictReader(f)
            for row in reader:
                group, _ = Group.objects.get_or_create(name=row.get('group'))
                Invite.objects.create(
                    event=self,
                    user=None,
                    primary_mail=row.get('email'),
                    first_name=row.get('first_name', ''),
                    last_name=row.get('last_name', ''),
                    section=group,
                    can_vote=bool(int(row.get('can_vote', '1'))),
                )

    def send_invites(self):
        messages = []

        for invite in self.invite_set.filter(state=INVITE_CREATED):
            messages.append((self.invite_mail_subject,
                             self.invite_mail_template.format(code=invite.code,
                                                              id=self.pk,
                                                              email=invite.primary_mail),
                             self.invite_mail_sender, [invite.primary_mail]))
        self.invite_set.all().update(state=INVITE_SENT)
        send_mass_mail(messages)

    def send_invites_with_pw(self):
        messages = []

        for invite in self.invite_set.filter(state=INVITE_CREATED):
            user, _ = get_user_model().objects.get_or_create(
                username=invite.primary_mail,
                defaults={
                    'email': invite.primary_mail,
                    'username': invite.primary_mail,
                    'first_name': invite.first_name,
                    'last_name': invite.last_name,
                },
            )
            password = secrets.token_urlsafe(12)
            user.set_password(password)
            user.save()
            invite.user = user
            invite.save()
            messages.append((self.invite_mail_subject,
                             self.invite_mail_template.format(
                                 email=user.email,
                                 password=password,
                                 id=invite.event.pk,
                             ), self.invite_mail_sender, [invite.primary_mail]))

        self.invite_set.all().update(state=INVITE_PROCESSED)
        send_mass_mail(messages)

    def send_reminder(self):
        messages = []

        for invite in self.invite_set.filter(state=INVITE_SENT):
            messages.append(
                (self.reminder_mail_subject,
                 self.reminder_mail_template.format(code=invite.code,
                                                    id=self.pk,
                                                    email=invite.primary_mail),
                 self.invite_mail_sender, [invite.primary_mail]))
        send_mass_mail(messages)

    def reset_invites(self):
        self.invite_set.filter(state=INVITE_CREATED).delete()

    def __str__(self):
        return self.title + ", " + self.date.strftime("%Y-%m-%d")

    class Meta:
        verbose_name = _("Anlass")
        verbose_name_plural = _("Anlässe")


class VideoLink(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("Name"))
    embed_code = models.TextField(verbose_name=_("Einbettungscode"))
    event = models.ForeignKey(Event, models.CASCADE, verbose_name=_("Event"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Video Link")
        verbose_name_plural = _("Video Links")


class ChatWindow(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("chat window"))
    event = models.ForeignKey(Event, models.CASCADE, verbose_name=_("Event"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Chat Window")
        verbose_name_plural = _("Chat Windows")
