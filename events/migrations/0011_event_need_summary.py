# Generated by Django 3.1.4 on 2021-04-19 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0010_event_mail_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='need_summary',
            field=models.BooleanField(default=False),
        ),
    ]
