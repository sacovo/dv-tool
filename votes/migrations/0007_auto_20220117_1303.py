# Generated by Django 3.2.11 on 2022-01-17 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('votes', '0006_auto_20210102_1356'),
    ]

    operations = [
        migrations.AddField(
            model_name='ballot',
            name='text_abstain',
            field=models.CharField(blank=True,
                                   max_length=40,
                                   verbose_name='Text Enthaltung'),
        ),
        migrations.AddField(
            model_name='ballot',
            name='text_no',
            field=models.CharField(blank=True, max_length=40, verbose_name='Nein Text'),
        ),
        migrations.AddField(
            model_name='ballot',
            name='text_yes',
            field=models.CharField(blank=True, max_length=40, verbose_name='Ja Text'),
        ),
    ]
