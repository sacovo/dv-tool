# Generated by Django 3.1.4 on 2020-12-31 20:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('votes', '0003_auto_20201231_2122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ballot',
            name='order',
            field=models.IntegerField(db_index=True, default=0, editable=False),
        ),
    ]
