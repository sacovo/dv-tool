# Generated by Django 3.1.4 on 2020-12-31 14:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('votes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ballot',
            name='in_progress',
            field=models.BooleanField(default=False),
        ),
    ]
