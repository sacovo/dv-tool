from django.contrib.auth.decorators import login_required, user_passes_test

# Create your views here.


@user_passes_test(lambda u: u.is_superuser)
def reset_vote_state(request, event_pk):
    pass


@user_passes_test(lambda u: u.is_superuser)
def save_result(request, event_pk, ballot_pk):
    pass


@login_required
def presentation_view(request, event_pk):
    pass
