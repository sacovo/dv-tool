from votes.models import Ballot
from django.contrib import admin
from adminsortable.admin import SortableAdmin, SortableTabularInline

# Register your models here.


class BallotInline(SortableTabularInline):
    model = Ballot
    fields = ('title', 'text_yes', 'text_no', 'text_abstain', 'processed', 'in_progress',
              'yes', 'no', 'abstain')
    readonly_fields = ('yes', 'no', 'abstain', 'processed', 'in_progress')

    extra = 0


@admin.register(Ballot)
class BallotAdmin(SortableAdmin):
    list_display = ('title', 'yes', 'no', 'abstain', 'in_progress', 'processed')
    list_filter = ['event', 'processed', 'in_progress']

    actions = ['close_ballot', 'open_ballot', 'reset_ballot']
    readonly_fields = ['yes', 'no', 'abstain']

    def close_ballot(self, request, queryset):

        for ballot in queryset.filter(processed=False)[:1]:
            ballot.save_ballot()

    def open_ballot(self, request, queryset):
        queryset.filter(processed=False).update(in_progress=True)

    def reset_ballot(self, request, queryset):
        queryset.update(yes=None,
                        no=None,
                        abstain=None,
                        in_progress=False,
                        processed=False)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(event__managers=request.user)
