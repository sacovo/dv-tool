from delegates.models import INVITE_PROCESSED, VOTE_ABSTAIN, VOTE_NO, VOTE_NONE, VOTE_YES

from adminsortable.models import SortableMixin
from events.models import Event
from django.db import models
from django.utils.translation import gettext as _

# Create your models here.

PUBLIC_FIELDS = ('title', 'yes', 'no', 'abstain', 'text_yes', 'text_no', 'text_abstain')


def get_open_vote_dict(event):
    vote = event.ballot_set.filter(in_progress=True).first()
    return get_vote_info(vote, event)


def get_vote_info(vote, event=None):
    if event is None:
        event = vote.event
    return {
        'vote_title':
            vote.title if vote else '',
        'vote_yes':
            event.invite_set.filter(
                vote=VOTE_YES,
                state=INVITE_PROCESSED,
                can_vote=True,
            ).count(),
        'vote_no':
            event.invite_set.filter(
                vote=VOTE_NO,
                state=INVITE_PROCESSED,
                can_vote=True,
            ).count(),
        'vote_abstain':
            event.invite_set.filter(
                vote=VOTE_ABSTAIN,
                state=INVITE_PROCESSED,
                can_vote=True,
            ).count(),
        'text_yes':
            vote.text_yes if vote else '',
        'text_no':
            vote.text_no if vote else '',
        'text_abstain':
            vote.text_abstain if vote else '',
    }


class Ballot(SortableMixin):
    title = models.CharField(max_length=280, verbose_name=_("Titel"))
    order = models.IntegerField(default=0, editable=False, db_index=True, verbose_name=_("Reihenfolge"))

    yes = models.IntegerField(null=True, blank=True, verbose_name=_("Ja"))
    no = models.IntegerField(null=True, blank=True, verbose_name=_("Nein"))
    abstain = models.IntegerField(null=True, blank=True, verbose_name=_("Enthaltung"))

    text_yes = models.CharField(max_length=40, blank=True, verbose_name=_("Ja Text"))
    text_no = models.CharField(max_length=40, blank=True, verbose_name=_("Nein Text"))
    text_abstain = models.CharField(max_length=40, blank=True, verbose_name=_("Text Enthaltung"))

    in_progress = models.BooleanField(default=False, verbose_name=_("Offen"))
    processed = models.BooleanField(default=False, verbose_name=_("Abgeschlossen"))
    event = models.ForeignKey(Event, models.CASCADE, verbose_name=_("Anlass"))

    def save_ballot(self):
        self.yes = self.event.invite_set.filter(
            vote=VOTE_YES,
            state=INVITE_PROCESSED,
            can_vote=True,
        ).count()
        self.no = self.event.invite_set.filter(
            vote=VOTE_NO,
            state=INVITE_PROCESSED,
            can_vote=True,
        ).count()
        self.abstain = self.event.invite_set.filter(
            vote=VOTE_ABSTAIN,
            state=INVITE_PROCESSED,
            can_vote=True,
        ).count()
        self.processed = True
        self.in_progress = False
        self.event.invite_set.all().update(vote=VOTE_NONE)
        self.save()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['order']

        verbose_name = _("Abstimmung")
        verbose_name_plural = _("Abstimmungen")
