import secrets
from django.db import models
from events.models import Event
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

# Create your models here.


class Message(models.Model):
    channel = models.CharField(max_length=30)
    message = models.CharField(max_length=280)

    timestamp = models.DateTimeField(auto_now_add=True)
    event = models.ForeignKey(Event, models.CASCADE)
    user = models.ForeignKey(get_user_model(), models.CASCADE)

    class Meta:
        ordering = ['-timestamp']


class Room(models.Model):
    name = models.CharField(max_length=120)

    owner = models.ForeignKey(get_user_model(), models.CASCADE)
    members = models.ManyToManyField(get_user_model(), related_name='+')

    link = models.CharField(max_length=255, default=secrets.token_urlsafe, verbose_name=_("Link"))
