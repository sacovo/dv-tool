import json
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Value as V
from django.db.models.functions import Concat
from chat.models import Room
from chat.forms import RoomForm
from django.http.response import JsonResponse
from django.shortcuts import render

# Create your views here.


@login_required
def all_users(request):
    return JsonResponse({
        'users': [{
            'value': x.id,
            'label': x.get_full_name()
        } for x in get_user_model().objects.all()]
    })


@login_required
def user_rooms(request):
    own_rooms = Room.objects.filter(owner=request.user).values('name', 'link', 'id')

    rooms = Room.objects.filter(members=request.user).values('name', 'link', 'id')

    return JsonResponse({
        'rooms': list(rooms),
        'owned': list(own_rooms),
    })


@csrf_exempt
@login_required
def room_members(request, room_pk):
    room = Room.objects.get(pk=room_pk)

    if room.owner == request.user:

        if request.method == 'POST':
            new_members = json.loads(request.body)['members']
            room.members.set(new_members)

        return JsonResponse({'members': list(room.members.values_list('id', flat=True))})


@csrf_exempt
@login_required
def create_room(request):
    form = RoomForm(request.POST)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.owner = request.user
        instance.save()

    return JsonResponse({'status': 'ok'})


@csrf_exempt
@login_required
def delete_room(request, room_pk):
    if request.method == 'POST':
        room = Room.objects.get(pk=room_pk)

        if room.owner == request.user:
            room.delete()

            return JsonResponse({'status': 'ok'})

    return JsonResponse({'status': 'err'})


@csrf_exempt
def update_room(request, room_pk):
    room = Room.objects.get(pk=room_pk)

    if request.POST and room.owner == request.user:
        form = RoomForm(request.POST, instance=room)
        if form.is_valid():
            form.save()

            return JsonResponse({'status': 'ok'})

    return JsonResponse({'status': 'err'})


@csrf_exempt
def leave_room(request, room_pk):

    if request.method == 'POST':
        Room.objects.get(pk=room_pk).members.remove(request.user)

        return JsonResponse({'status': 'ok'})

    return JsonResponse({'status': 'err'})
