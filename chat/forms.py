from django.contrib.auth import get_user_model
from chat.models import Room
from django import forms


class UserChoiceField(forms.ModelMultipleChoiceField):

    def label_from_instance(self, obj):
        return obj.get_full_name()


class RoomForm(forms.ModelForm):

    class Meta:
        model = Room
        fields = ['name']


class UserSelectForm(forms.Form):
    members = UserChoiceField(get_user_model().objects)

    class Meta:
        model = Room
        fields = ['members']
