from django.urls import path
from chat import views

app_name = "chat"

urlpatterns = [
    path('rooms/', views.user_rooms, name="user_rooms"),
    path('creae/', views.create_room, name="create_room"),
    path('delete/<int:room_pk>/', views.delete_room, name="delete_room"),
    path("update/<int:room_pk>/", views.update_room, name="update_room"),
    path("leave/<int:room_pk>/", views.leave_room, name="leave_room"),
    path("members/<int:room_pk>/", views.room_members, name="room_members"),
    path('users/', views.all_users, name="all_users"),
]
