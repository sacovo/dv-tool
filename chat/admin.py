from chat.models import Message, Room
from django.contrib import admin

# Register your models here.


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['user', 'channel', 'event', 'timestamp']

    list_filter = ['event', 'channel', 'user']

    search_fields = ['message', 'user']

    autocomplete_fields = ['event']

    readonly_fields = ['user', 'channel']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(event__managers=request.user)


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    model = Room
