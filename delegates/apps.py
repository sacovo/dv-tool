from django.apps import AppConfig


class DelegatesConfig(AppConfig):
    name = 'delegates'
