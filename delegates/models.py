import secrets

from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.db import transaction

# Create your models here.

INVITE_CREATED = 0
INVITE_SENT = 1
INVITE_PROCESSED = 2

INVITE_STATES = (
    (INVITE_CREATED, _("erstellt")),
    (INVITE_SENT, _("verschickt")),
    (INVITE_PROCESSED, _("verarbeitet")),
)

VOTE_NONE = -1
VOTE_ABSTAIN = 0
VOTE_YES = 1
VOTE_NO = 2

VOTE_CHOICES = (
    (VOTE_NONE, _("keine Stimme")),
    (VOTE_ABSTAIN, _("Enthaltung")),
    (VOTE_YES, _("Ja")),
    (VOTE_NO, _("Nein")),
)


class Invite(models.Model):
    event = models.ForeignKey("events.Event", models.CASCADE, verbose_name=_("Event"))
    user = models.ForeignKey(get_user_model(), models.SET_NULL, blank=True, null=True, verbose_name=_("Benutzer*in"))

    primary_mail = models.EmailField(verbose_name=_("Primäre E-Mail Adresse"))
    code = models.CharField(max_length=255, default=secrets.token_urlsafe, verbose_name=_("Code"))

    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)

    state = models.IntegerField(choices=INVITE_STATES, default=0, verbose_name=_("Status"))

    can_vote = models.BooleanField(default=True, verbose_name=_("Stimmberechtigt"))

    vote = models.IntegerField(choices=VOTE_CHOICES, default=-1, verbose_name=_("Stimme"))

    section = models.ForeignKey("auth.Group", models.CASCADE, verbose_name=_("Sektion"))

    def register_user(self, user):
        with transaction.atomic():
            self.user = user
            self.state = INVITE_PROCESSED
            user.groups.add(self.section)
            user.save()
            self.save()

    class Meta:
        ordering = ['section', 'pk']

        verbose_name = _("Einladung")
        verbose_name_plural = _("Einladungen")
