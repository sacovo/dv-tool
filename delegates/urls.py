from django.urls import path

from delegates import views

app_name = "delegates"

urlpatterns = (
    path("<uuid:event_pk>/", views.landing, name="landing"),
    path("register/<uuid:event_pk>/", views.registration_view, name="register"),
    path("join/<uuid:event_pk>/", views.join_event, name="join"),
)
