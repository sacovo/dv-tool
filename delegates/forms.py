from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from delegates.models import INVITE_SENT, Invite
from django.utils.translation import gettext_lazy as _
from django import forms


class RegistrationForm(forms.Form):
    first_name = forms.CharField(label=_("Vorname"))
    last_name = forms.CharField(label=_("Nachname"))

    email = forms.EmailField(label=_("E-Mail"))
    code = forms.CharField(label=_("Code"))

    password = forms.CharField(widget=forms.PasswordInput, label=_("Passwort"))
    confirm_password = forms.CharField(widget=forms.PasswordInput, label=_("Passwort wiederholen"))

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event')

        super().__init__(*args, **kwargs)

    def clean(self):
        data = super().clean()

        if data['password'] != data['confirm_password']:
            raise forms.ValidationError(_("Passwords don't match."))

        validate_password(data['password'])

        if get_user_model().objects.filter(email=data['email']).exists():
            raise forms.ValidationError(_("already registered"))

        try:
            invite = self.event.invite_set.get(code=data['code'], state=INVITE_SENT)
            data['invite'] = invite

        except Invite.DoesNotExist:
            raise forms.ValidationError(_("invalid or old code provided"))

        return data


class JoinForm(forms.Form):
    code = forms.CharField(label=_("Code"))

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop('event')
        super().__init__(*args, **kwargs)

    def clean(self):
        data = super().clean()

        try:
            invite = self.event.invite_set.get(code=data['code'], state=INVITE_SENT)
            data['invite'] = invite

        except Invite.DoesNotExist:
            raise forms.ValidationError(_("invalid or old code provided"))

        return data
