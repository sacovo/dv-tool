import secrets
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.core.mail import send_mass_mail
from delegates import models

# Register your models here.


@admin.register(models.Invite)
class InviteAdmin(admin.ModelAdmin):
    list_display = ('primary_mail', 'user', 'section', 'state')

    search_fields = ['primary_mail']

    list_filter = ['event', 'state']

    readonly_fields = ['vote']

    actions = ['send_invite', 'send_invites_with_pw']

    autocomplete_fields = ['event']

    def send_invite(self, request, queryset):
        messages = []
        for invite in queryset:
            messages.append(
                (invite.event.invite_mail_subject,
                 invite.event.invite_mail_template.format(code=invite.code,
                                                          id=invite.event.pk,
                                                          email=invite.primary_mail),
                 invite.event.invite_mail_sender, [invite.primary_mail]))
        queryset.update(state=models.INVITE_SENT)

        send_mass_mail(messages)

    def send_invites_with_pw(self, request, queryset):
        messages = []
        for invite in queryset:
            user, _ = get_user_model().objects.get_or_create(
                username=invite.primary_mail,
                defaults={
                    'email': invite.primary_mail,
                    'username': invite.primary_mail,
                    'first_name': invite.first_name,
                    'last_name': invite.last_name,
                },
            )
            invite.user = user
            invite.state = models.INVITE_PROCESSED

            password = secrets.token_urlsafe(12)
            user.set_password(password)
            user.save()
            invite.save()

            messages.append((invite.event.invite_mail_subject,
                             invite.event.invite_mail_template.format(
                                 email=user.email,
                                 password=password,
                                 id=invite.event.pk,
                             ), invite.event.invite_mail_sender, [invite.primary_mail]))
        queryset.update(state=models.INVITE_SENT)

        send_mass_mail(messages)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        return qs.filter(event__managers=request.user)
