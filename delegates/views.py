from django.contrib import messages
from delegates.models import INVITE_PROCESSED, Invite
from django.contrib.auth import get_user, get_user_model, login
from events.models import Event
from delegates.forms import JoinForm, RegistrationForm
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.backends import ModelBackend
from django.utils.translation import gettext as _
from django.urls import reverse

# Create your views here.


def landing(request, event_pk):
    if request.user.is_authenticated:
        return join_event(request, event_pk)

    if code := request.GET.get('code'):
        if Invite.objects.filter(code=code, state=INVITE_PROCESSED).exists():
            return redirect(
                reverse('login') + "?next=" +
                reverse("delegates:join", kwargs={'event_pk': event_pk}) + '?code=' +
                code)
    return registration_view(request, event_pk)


def registration_view(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    form = RegistrationForm(event=event, initial=request.GET)

    if request.method == 'POST':
        form = RegistrationForm(request.POST, event=event)

        if form.is_valid():
            data = form.cleaned_data
            user = get_user_model().objects.create_user(
                username=data['email'],
                email=data['email'],
                first_name=data['first_name'],
                last_name=data['last_name'],
                password=data['password'],
            )

            messages.success(request, _("Registration erfolgreich!"))

            data['invite'].register_user(user)

            login(request, user, backend='django.contrib.auth.backends.ModelBackend')

            return redirect("events:event_list")

    return render(request, 'delegates/registration_view.html', {
        'form': form,
        'event': event,
        'code': request.GET.get('code', ''),
    })


@login_required
def join_event(request, event_pk):
    event = Event.objects.get(pk=event_pk)

    if event.invite_set.filter(user=request.user).exists():
        messages.add_message(request, messages.WARNING, _("Du bist bereits bei diesem Event dabei!"))
        return redirect("events:event_detail", event.pk)

    form = JoinForm(event=event, initial=request.GET)

    if request.method == 'POST':
        form = JoinForm(request.POST, event=event)

        if form.is_valid():
            data = form.cleaned_data

            data['invite'].register_user(request.user)

            return redirect("events:event_detail", event.pk)

    return render(request, 'delegates/join_event.html', {
        'form': form,
        'event': event,
    })
