# Generated by Django 3.1.4 on 2020-12-31 12:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import secrets


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('events', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invite',
            fields=[
                ('id',
                 models.AutoField(auto_created=True,
                                  primary_key=True,
                                  serialize=False,
                                  verbose_name='ID')),
                ('primary_mail', models.EmailField(max_length=254)),
                ('code', models.CharField(default=secrets.token_urlsafe, max_length=255)),
                ('state',
                 models.IntegerField(choices=[(0, 'created'), (1, 'sent'),
                                              (2, 'processed')],
                                     default=0)),
                ('vote',
                 models.IntegerField(choices=[(-1, 'not voted'), (0, 'abstain'),
                                              (1, 'yes'), (2, 'no')],
                                     default=-1)),
                ('event',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                   to='events.event')),
                ('section',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                   to='auth.group')),
                ('user',
                 models.ForeignKey(blank=True,
                                   null=True,
                                   on_delete=django.db.models.deletion.SET_NULL,
                                   to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
