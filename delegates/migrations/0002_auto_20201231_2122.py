# Generated by Django 3.1.4 on 2020-12-31 20:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('delegates', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='invite',
            options={'ordering': ['section', 'pk']},
        ),
    ]
