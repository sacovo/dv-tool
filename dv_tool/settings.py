"""
Django settings for dv_tool project.

Generated by 'django-admin startproject' using Django 3.1.4.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from pathlib import Path
import os
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY", "change-me")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = int(os.environ.get("DEBUG", default=0))

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    "django_celery_beat",
    "django_celery_results",
    "adminsortable",
    "bootstrap5",
    "delegates",
    "events",
    "speakers",
    "votes",
    "chat",
    "axes",
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'axes.middleware.AxesMiddleware',
]

LOGO_URL = os.environ.get("LOGO_URL", '/static/logo.png')


def logo_url(request):
    return {'logo_url': LOGO_URL}


def jitsi_settings(request):
    return {
        'JITSI_DOMAIN': JITSI_DOMAIN,
        'JITSI_PREFIX': JITSI_PREFIX,
    }


ROOT_URLCONF = 'dv_tool.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates/'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.static',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'dv_tool.settings.jitsi_settings',
                'speakers.models.request_states',
                'dv_tool.settings.logo_url',
            ],
        },
    },
]

WSGI_APPLICATION = 'dv_tool.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": os.environ.get("SQL_ENGINE", "django.db.backends.sqlite3"),
        "NAME": os.environ.get("POSTGRES_DB", os.path.join(BASE_DIR, "db.sqlite3")),
        "USER": os.environ.get("POSTGRES_USER", "user"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD", "password"),
        "HOST": os.environ.get("SQL_HOST", "localhost"),
        "PORT": os.environ.get("SQL_PORT", "5432"),
    }
}

LOGIN_REDIRECT_URL = "/"

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
            'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    'axes.backends.AxesBackend',
    'django.contrib.auth.backends.ModelBackend',
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'de'

TIME_ZONE = "Europe/Zurich"

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = ["locale/"]

STATICFILES_DIRS = [
    BASE_DIR / "dv_tool/static",
]

STATIC_ROOT = "static/"
STATIC_URL = "/static/"

MEDIA_ROOT = "media/"
MEDIA_URL = "/media/"

UPLOAD_ROOT = 'uploads/'

LANGUAGES = [
    ("de", _("German")),
    ("fr", _("French")),
    ("it", _("Italian")),
    ("en", _("English")),
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

CELERY_TIMEZONE = TIME_ZONE
CELERY_BROKER_URL = "redis://redis:6379"

CELERY_RESULT_BACKEND = "django-db"
CELERY_CACHE_BACKEND = "django-cache"

DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL", "webmaster@localhost")
SERVER_EMAIL = os.environ.get("SERVER_EMAIL", "root@localhost")

EMAIL_HOST = os.environ.get("SMTP_HOST", "smtp")
EMAIL_PORT = int(os.environ.get("SMTP_PORT", "25"))

EMAIL_USE_TLS = int(os.environ.get("SMTP_TLS", "0"))
EMAIL_USE_SSL = int(os.environ.get("SMTP_SSL", "0"))

EMAIL_HOST_USER = os.environ.get("SMTP_USER", "user")
EMAIL_HOST_PASSWORD = os.environ.get("SMTP_PASSWORD", "pw")

DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL", "webmaster@localhost")
SERVER_EMAIL = os.environ.get("SERVER_EMAIL", "root@localhost")

INTERNAL_IPS = ['172.23.0.1']

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.PyMemcacheCache",
        "LOCATION": "cache:11211",
    }
}

AXES_FAILURE_LIMIT = 30
AXES_COOLOFF_TIME = 1

AXES_PROXY_COUNT = int(os.environ.get("PROXY_COUNT", "0"))
AXES_ONLY_USER_FAILURES = True
AXES_RESET_ON_SUCCESS = True

SITE_ID = int(os.environ.get("SITE_ID", '2'))

JITSI_DOMAIN = os.environ.get("JITSI_DOMAIN", 'meet.sp-ps.ch')
JITSI_PREFIX = os.environ.get("JITSI_PREFIX", '')

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
